<?php
    function temperaturasList($index = NULL ,$limit = NULL, $order = FALSE){
        if($order){
            $order = "temperatura desc";
        }

        return consultDatabase("temperaturas",["*"],NULL, NULL, $order,$index, $limit);
    }

    function totalMediciones(){
        $total = 0;
        $response = consultDatabase("temperaturas",["COUNT(*) as total"]);
        if(count($response) > 0){
            $total = $response[0]["total"];
        }
        return $total;
    }

    function averageTemp($muestreo){
        $sum = 0;
        foreach($muestreo as $fila){
            $temp = $fila["temperatura"];
            $num = floatval($temp);
            $sum = $sum + $num;
        }

        return $sum / count($muestreo);
    }
