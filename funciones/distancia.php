<?php
    function distanciasList($index = NULL ,$limit = NULL){
        return consultDatabase("distancias",["*"],NULL, NULL, NULL,$index, $limit);
    }

    function totalProximidadEvents(){
        $total = 0;
        $response = consultDatabase("distancias",["COUNT(*) as total"]," evento LIKE '%proximidad%';");
        if(count($response) > 0){
            $total = $response[0]["total"];
        }
        return $total;
    }

    function totalFueraRangoEvents(){

        $total = 0;
        $response = consultDatabase("distancias",["COUNT(*) as total"]," evento LIKE '%fuera de rango%';");
        if(count($response) > 0){
            $total = $response[0]["total"];
        }
        return $total;

    }
    
    function totalEvents(){
        $total = 0;
        $response = consultDatabase("distancias",["COUNT(*) as total"]);
        if(count($response) > 0){
            $total = $response[0]["total"];
        }
        return $total;
    }