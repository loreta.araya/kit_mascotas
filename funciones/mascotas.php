<?php
    function mascotasList($index = NULL ,$limit = NULL){
        return consultDatabase("mascotas",["*"],NULL, NULL, NULL,$index, $limit);
    }

    function MascotaCreate($nombre,$fecha_nac,$especie){
        return createDataBase(
            "mascotas",
            [
                "nombre" => $nombre,
                "fecha_nac" => $fecha_nac,
                "especie" => $especie
            ]
        );
    }
