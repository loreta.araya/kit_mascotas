<?php

    //Establece una conexion a la base de datos
    function conectDataBase(){
        $user = "root";
        $pswd = "";
        $host = "localhost";
        $dbname = "kit_mascotas";

        try{

            $conexion = new PDO(
                "mysql:host=".$host.";dbname=".$dbname, 
                $user,
                $pswd,
                array(
                    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
                )
            );

        } catch(PDOException $e){

            //Manejo de error en la conexion
            $_SESSION[date("Y-m-d_H:i:s")] = $e;
            $conexion = FALSE;
        }

        return $conexion;
    }

    //Crear registro en base de datos
    function createDataBase($table = "",$data){
        $conexion = conectDataBase();
        $response = FALSE;

        $fields = implode(",",array_keys($data));
        $data = array_map(function($x){
            return '"'.$x.'"';
        },$data);

        $data = implode(",",$data);

        $sql = "INSERT INTO ".$table."(".$fields.") VALUES(".$data.");";

        echo $sql;

        try{
            //Manejo de error de la conexion
            if($conexion != FALSE){

                $query = $conexion->prepare($sql);
                $response = $query->execute();
            }

        } catch(PDOException $e){

            //Manejo de error en la conexion
            $_SESSION[date("Y-m-d_H:i:s")] = $e;
        }

        return $response;
    }

    //Consulta de datos
    function consultDatabase($table = "", $fields = [], $conditions = "", $joins = "", $order = "",$index = "", $limit = ""){

        $conexion = conectDataBase();
        $response = [];

        $fields = implode(" ,",$fields);
        $sql = "SELECT ".$fields." FROM ".$table." ";

        //Conexiones entre tablas
        if($joins != ""){
            $sql .= $joins."";
        }

        //Condiciones de consultas
        if($conditions != ""){
            $sql .= "WHERE ".$conditions."";
        }

        //Ordenado por
        if($order != null){
            $sql .= ' ORDER BY '.$order;
        }

        //Paginacion
        if($index != "" && $limit != ""){
            $sql .= "LIMIT ".$limit." OFFSET ".$index;
        }

        //Realizar Consulta
        try {

            //Manejo de error de la conexion
            if($conexion != FALSE){

                $query = $conexion->prepare($sql);

                if($query->execute()){
                    $response = $query->fetchAll(PDO::FETCH_ASSOC);
                }    
            }

        } catch(PDOException $e){

            //Manejo de error en la conexion
            $_SESSION[date("Y-m-d_H:i:s")] = $e;
        }

        return $response;
    }