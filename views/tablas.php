<div id="awa">
    <div id="efe">
        <h1><?php echo ucfirst($title);?></h1>
    </div>
    <pre><?php echo $descripcion;?></pre>
</div>

<!-- tabla -->
<div id="tabla2">
    <?php if (count($tabla) > 0):?>
    <table>
        <!-- Titulos -->
        <thead>
            <tr>
            <?php $titulosTabla = array_keys($tabla[0]);?> 
            <?php foreach($titulosTabla as $i => $tituloTabla):?>
                <?php $tituloTabla = ($i == 0)? "id":  $tituloTabla;?>
                <?php $tituloTabla = ($tituloTabla == "fecha_nac")? "Fecha de Nacimiento":  $tituloTabla;?>
                <th><?php echo strtoupper($tituloTabla);?></th>
            <?php endforeach;?>
            </tr>
        </thead>

        <!-- Datos -->
        <?php foreach($tabla as $fila):?>
        <tr>
            <?php foreach($fila as $fieldTable):?>
            <td><?php echo strtolower($fieldTable);?></td>
            <?php endforeach;?>
        </tr>
        <?php endforeach;?>
    </table>
    <?php endif;?>
</div>