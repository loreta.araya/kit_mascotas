<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php echo $title;?></title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
    <?php
        require_once "views/menu.php"
    ?>
    <?php
        require_once $content;
    ?>
</body>
</html>