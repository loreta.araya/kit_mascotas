<link rel ="stylesheet" type="text/css" href="css/style4.css">
<div id="ewe">
    <div id="titulo1">
        <h1>NUESTRO PROYECTO</h1>
    </div>
    <div id="parrafo">
        <p>Como futuros ingenieros/a civiles en informátciso se nos a dado un encargo basado en "Internet of things" (internet de las cosas)
            con el cual debemos brindar una solución efectiva ante una problemática apoyados en el uso de software y hardware.
            Dicho problema consiste en diseñar, para una posterior implementación, un "Kit Hogareño" basado en Arduino, con el cual se espera poder regular
            la temperatura de un ambeinte cerrado como un departamento, una habitación o una casa, pero ¿Como cumplir esta tarea?
            Con mecanismos que faciliten tanto la apertura como cierre de puertas y/o ventanas de un recinto para una mjeor ventilación.
            Por otro lado, también se nos solicita que el mecanismo a realizar permita el ingreso y salida de mascotas según lo permitan sus dueños, para esto se dispone de puertas "gateras"
            que funcionan según la proximidad del felino a la puerta en sí mediante el usp de sensores. Como información adicional, se detalla que este proyecto dirigido por la
            SUBDERE será proporcionado a diversas familias de la región como parte de un plan piloto con posibilidades de masificación.
        </p>
    </div>
</div>