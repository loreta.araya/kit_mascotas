<?php

    require_once "funciones/bbdd.php";
    require_once "funciones/distancia.php";
    require_once "funciones/temperatura.php";
    require_once "funciones/mascotas.php";
    session_start();

    $page = (isset($_GET["page"]))? $_GET["page"]: "home";

    switch($page){

        case "home":
            $title = "Inicio";
            $content = "views/inicio.php";
            require_once "views/main.php";
            break;

        case "distancia":
            $title = "Eventos Destacados";
            $totalRegistros = totalEvents();
            $eventosFueraRango = totalFueraRangoEvents();
            $EventosProxi = totalProximidadEvents();
            $tabla = distanciasList();
            $descripcion = " 
                A la fecha se han detectado <b>".$totalRegistros."</b> Eventos.
                Se han detectado <b>".$eventosFueraRango."</b> eventos de temperatura fuera del rango
                Se han detectados <b>".$EventosProxi."</b> eventos de proximidad a la puerta gatera
            ";
            $content = "views/tablas.php";
            require_once "views/main.php";
            break;

        case "temperatura":
            $title = "Temperatura";
            $totalMediciones = totalMediciones();

            $muestreo = temperaturasList(NULL,NULL,TRUE);

            $highTemp = $muestreo[0]["temperatura"];
            $lowTemp =  $muestreo[count($muestreo)-1]["temperatura"];
            $averageTemp = round(averageTemp($muestreo), 2);

            $tabla = temperaturasList();
            $descripcion = " 
                A la fecha se han realizado <b>".$totalMediciones."</b> Mediciones.
                La temperatura mas alta registrada es de <b>".$highTemp."</b>
                La temperatura mas baja registrada es de <b>".$lowTemp."</b>
                La temperatura promedio es de <b>".$averageTemp."</b>
            ";
            $content = "views/tablas.php";
            require_once "views/main.php";
            break;

        case "crear_mascotas":
            $title = "Crear Mascotas";
            if(isset($_POST["task"])){
                MascotaCreate(
                    $_POST["nombre"],
                    $_POST["fecha_nac"],
                    $_POST["especie"]
                );
                header("location:index.php?page=mascotas");

            }else{
                $content = "views/formularioMascotas.php";
                require_once "views/main.php";
            }
            break;
        
        case "mascotas":
            $title = "Mascotas";
            $tabla = mascotasList();
            $descripcion = "Listado de mascotas";
            $content = "views/tablas.php";
            require_once "views/main.php";

            break;

        default:
            echo "404 - Esta pagina no existe";
            break;
    }
    session_destroy();